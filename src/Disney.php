<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();
        //gets list of all actors
        $actors = $this->xpath->query('Actors/Actor');
        foreach ($actors as $actor) { //gos trough all actors
          $actorChilds = $actor->childNodes;
          $actorID = $actor->getAttribute('id'); //gets actor-id
          $actorN = $actorChilds->item(1)->nodeValue; //gets actorname
          $arrayName = array(); //creates new array for each actor
          $movies = $this->xpath->query("Subsidiaries/Subsidiary/Movie[Cast/Role[@actor='$actorID']]");
          //finds the movies where the actor has a part
          foreach ($movies as $movie) {
            $movieChilds = $movie->childNodes; //gets the childs of each movie
            $movieT = $movieChilds->item(1)->nodeValue; //gets the title
            $movieY = $movieChilds->item(3)->nodeValue; //gets the year

            $nodeR = $this->xpath->query("Subsidiaries/Subsidiary/Movie[Name='$movieT']/Cast/Role[@actor='$actorID']/@name");
            //finds the role-node where the movie and actor are present
            $movieR = $nodeR[0]->nodeValue; //retrives the role
            $arrayName[] = "As {$movieR} in {$movieT} ({$movieY})"; //adds to the array of the actor
          }
          $result[$actorN] = $arrayName; //adds the array to the result-array
        }
          return $result; //returns the result
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
        $actors = $this->xpath->query('Actors/Actor'); //finds all actors
        foreach ($actors as $actor) {
          $actorID = $actor->getAttribute('id'); //gets actor id
          $movies = $this->xpath->query("Subsidiaries/Subsidiary/Movie[Cast/Role[@actor='$actorID']]");
          //get list of all movies where the actor has a role
          if($movies->length == 0){ //if the actor has no roles
            $actor->parentNode->removeChild($actor); //goes to the parent and removes the child:$actor
        }
      }
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        $castList = $this->xpath->query("Subsidiaries/Subsidiary[@id='$subsidiaryId']/
        Movie[Name='$movieName' and Year='$movieYear']/Cast")[0];
        //gets the list of the cast
        $role = $this->doc->createElement("Role"); //creates a role element
        $role->setAttribute('name', $roleName); //sets rolename
        $role->setAttribute('actor', $roleActor); //sets actorname
        $castList->appendChild($role); //appends child to the castlist
    }
}
?>
